from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import NewReceiptForm, NewCategoryForm, NewAccountForm


# create a view that will get all instances of the Receipt model
# and put them in the context for the template
@login_required
def show_all_receipts(request):
    receipts = Receipt.objects.filter(
        purchaser__username=request.user.username
    )
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_new_receipt(request):
    if request.method == "POST":
        form = NewReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = NewReceiptForm()

        context = {
            "form": form,
        }
        return render(request, "receipts/create_receipt.html", context)


# CATEGORY VIEWS


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = NewCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = NewCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


# ACCOUNT VIEWS


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = NewAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = NewAccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
