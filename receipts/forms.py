from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


# new receipt form
class NewReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


# new Category Form:


class NewCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


# new account form:


class NewAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
