from django.db import models
from django.conf import settings


# Create your models here.

# ExpenseCategory Model: value applied to receipts
#   like "gas" or "entertainment"
# name: max length 150
# owner: foreignkey to User model with
#   related name "categories"
#   cascade deletion


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )


# Account: the way we paid for it, credit card/bank acct
# name: max length 100
# number: contains characters max len 20
# owner: foriegn key to User model w/
#   related name "accounts"
#   cascade deletion


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )


# Receipt model: primary thing we are keeping track of
#   vendor: maxlen 200
#   total: decimalfield with 3 places, max 10 digits
#   tax: decimalfield with 3 places, max 10 digits
#   date: date and time of transaction taking place
#   purchaser: foreignkey to User
#       related name = "receipts"
#       cascade deletion
#   category: foreignkey to ExpenseCategory model
#       related name = receipts
#       cascade deletion
#   account: foreignkey to Account model
#       related name = receipts
#       casecade deletion
#       allowed to be null
class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(decimal_places=3, max_digits=10)
    tax = models.DecimalField(decimal_places=3, max_digits=10)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        "ExpenseCategory", related_name="receipts", on_delete=models.CASCADE
    )
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
