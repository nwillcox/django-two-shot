from django import forms


# log in, login, sign in, signin
class LogIn_Form(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )


# sign up, signup, create user, createuser
class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(),
    )
