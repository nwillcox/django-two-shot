from django.urls import path
from accounts.views import UserLogIn, UserLogOut, UserSignUp


urlpatterns = [
    path("login/", UserLogIn, name="login"),
    path("logout/", UserLogOut, name="logout"),
    path("signup/", UserSignUp, name="signup"),
]
